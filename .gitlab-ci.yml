image: node:stretch

variables:
  DISTDIR: ${CI_PROJECT_DIR}/dist/
  REPORTDIR: ${CI_PROJECT_DIR}/.public/
  DOCDIR: ${CI_PROJECT_DIR}/.public/docs/
  SOURCEDIR: ${CI_PROJECT_DIR}

stages:
  - prepare
  - build
  - test
  - deploy

.provisioning: &with_provisioning
  before_script:
    - mkdir -p .public/shields
    - echo ${SHIELDS} | xargs -n 1 sh -c 'curl -sL https://img.shields.io/badge/$1-unknown-lightgray.svg > .public/shields/$1.svg' curl
    - >-
      { grep -rlZ "#:# Jobs:.*\bbase\b.*#:#" provisioners/ || [ $? -lt 2 ]; }
      | sort -z
      | xargs -0 -n 1 bash
    - >-
      { grep -rlZ "#:# Jobs:.*\b${CI_JOB_NAME}\b.*#:#" provisioners/ || [ $? -lt 2 ]; }
      | sort -z
      | xargs -0 -n 1 bash

prepare:
  image: alpine:latest
  stage: prepare
  before_script:
    - apk add --update curl
  script:
    - mkdir -p .public/shields
    - curl -sL https://img.shields.io/badge/commit-${CI_COMMIT_SHA:0:8}-blue.svg > .public/shields/commit.svg
  artifacts:
    paths:
      - .public

dist:
  stage: build
  variables:
    SHIELDS: build
  <<: *with_provisioning
  script:
    - make dist
    - >-
      tar --create -f ${CI_PROJECT_NAME}.latest.tar dist package.json LICENSE README.md CONTRIBUTING.md
      && cd src && tar --append -f ../${CI_PROJECT_NAME}.latest.tar *
      && cd ..
      && gzip -c ${CI_PROJECT_NAME}.latest.tar > .public/${CI_PROJECT_NAME}.latest.tar.gz
      && bzip2 -c ${CI_PROJECT_NAME}.latest.tar > .public/${CI_PROJECT_NAME}.latest.tar.bz2
      && curl -sL https://img.shields.io/badge/build-`date -u +'%Y--%m--%d%%20%H%%3A%M%Z'`-blue.svg > .public/shields/build.svg
  artifacts:
    when: always
    paths:
      - .public

lint:
  stage: test
  variables:
    SHIELDS: style
  <<: *with_provisioning
  dependencies: []
  allow_failure: true
  script:
    - make lint-ci
  after_script:
    - >-
      cat .public/lint.out
      | awk 'BEGIN { errors = 0 } /[0-9]+ +problems?/ { errors += $2 } END { color = (errors == 0) ? "brightgreen" : (errors <= 3) ? "orange" : (errors <= 5) ? "yellow" : "red"; print "https://img.shields.io/badge/style-" errors "%20issue"(errors != 1 ? "s" : "")"-" color ".svg" }'
      | xargs curl -sL > .public/shields/style.svg
    - rm .public/lint.out
  artifacts:
    when: always
    paths:
      - .public

test:
  stage: test
  variables:
    SHIELDS: "tests coverage"
  <<: *with_provisioning
  dependencies: []
  script:
    - make test-ci
  after_script:
    - >-
      cat .public/tap.out
      | awk '/# pass/ { pass = $3 } /# tests/ { tests = $3 } END { color = (pass == tests) ? "brightgreen" : "red"; print "https://img.shields.io/badge/tests-" pass "%20%2F%20" tests "-" color ".svg" }'
      | xargs curl -sL > .public/shields/tests.svg
    - rm .public/tap.out
    - >-
      cat .public/coverage.out
      | awk '/Branches +: +[0-9.]+%/ { coverage = 0 + substr($3, 1, length($3)-1) } END { color = (coverage > 95.0) ? "brightgreen" : (coverage > 90) ? "orange" : (coverage > 85) ? "yellow" : "red"; print "https://img.shields.io/badge/coverage-" coverage"%25""-" color ".svg" }'
      | xargs curl -sL > .public/shields/coverage.svg
    - rm .public/coverage.out
  artifacts:
    when: always
    paths:
      - .public

doc:
  stage: test
  variables:
    LC_ALL: "C.UTF-8"
    LANG: "C.UTF-8"
  <<: *with_provisioning
  allow_failure: true
  script:
    - make docs
  artifacts:
    when: always
    expire_in: 15 minutes
    paths:
      - .public

pages:
  image: alpine:latest
  stage: deploy
  when: always
  dependencies:
    - prepare
    - dist
    - doc
    - lint
    - test
  script:
    - mv .public public
  artifacts:
    paths:
      - public
  only:
    - master
