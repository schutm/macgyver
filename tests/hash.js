/* eslint-disable no-magic-numbers */

import { djb2 } from '../src/hash';

test('[djb2] Return the correct result for an empty string', () => {
	expect(djb2('')).toBe(5381);
});

test('[djb2] Hashes correctly', () => {
	expect(djb2('test')).toBe(2090756197);
});
