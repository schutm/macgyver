import { matchAll } from '../src/regex';

test('[matchall] Non-matching regex results in empty array', () => {
	expect(matchAll(/aa(b+)cc/g, 'aacc')).toEqual([]);
});

test('[matchall] A match results in an array of matched patterns', () => {
	expect(matchAll(/aa(b+)cc/g, 'aabbcc')).toEqual([['aabbcc', 'bb']]);
	expect(matchAll(/aa(b+)cc/g, 'aabbccaabbcc')).toEqual([['aabbcc', 'bb'], ['aabbcc', 'bb']]);
});
