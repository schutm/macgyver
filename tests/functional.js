/* eslint-disable no-magic-numbers */

import { curry, partial } from '../src/functional';

function add(a, b, c) {
	return a + b + c;
}

test('[curry] Returns a function', () => {
	expect(curry(add)).toBeInstanceOf(Function);
});

test('[curry] Return proper result with exact number of arguments as expected', () => {
	const curriedAdd = curry(add);

	expect(curriedAdd(1, 2, 3)).toBe(6);
});

test('[curry] Return proper result with more arguments than expected', () => {
	const curriedAdd = curry(add);

	expect(curriedAdd(1, 2, 3, 4, 5, 6, 7)).toBe(6);
});

test('[curry] Return chainable invocations', () => {
	const curriedAdd = curry(add);

	expect(curriedAdd(1)(2)(3)).toBe(6);
	expect(curriedAdd(1, 2)(3)).toBe(6);
	expect(curriedAdd(1)(2, 3)).toBe(6);
});

test('[curry] Create multiple curry functions', () => {
	const curriedAdd1 = curry(add);
	const curriedAdd2 = curry(add);

	expect(curriedAdd1).toBeInstanceOf(Function);
	expect(curriedAdd2).toBeInstanceOf(Function);
	expect(curriedAdd1).not.toBe(curriedAdd2);
});

test('[partial] Return a function', () => {
	expect(partial(add)).toBeInstanceOf(Function);
});

test('[partial] Return a function with the number of arguments as expected', () => {
	const partialAdd = partial(add);

	expect(partialAdd(1, 2, 3)).toBe(6);
});

test('[partial] Return a chainable invocation', () => {
	expect(partial(add)(1, 2, 3)).toBe(6);
	expect(partial(add, 1)(2, 3)).toBe(6);
	expect(partial(add, 1, 2)(3)).toBe(6);
	expect(partial(add, 1, 2, 3)()).toBe(6);
});

test('[partial] Do not keep on chaining', () => {
	function chainTest() {
		const partialAdd = partial(add, 1);
		partialAdd(2)(3);
	}

	expect(chainTest).toThrowError(TypeError);
	expect(chainTest).toThrowError('partialAdd(...) is not a function');
});
