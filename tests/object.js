/* eslint-disable no-magic-numbers */

/**
 * Tests based on deepmerge module https://github.com/KyleAMathews/deepmerge
 */

import { deepmerge } from '../src/object';

const monday = new Date('2016-09-27T01:08:12.761Z');
const tuesday = new Date('2016-09-28T01:18:12.761Z');

test('[deepmerge] Add keys in target that do not exist at the root', () => {
	const source = { key1: 'value1', key2: 'value2' };
	const target = {};

	const result = deepmerge(target, source);

	expect(target).toEqual({});
	expect(result).toEqual(source);
});

test('[deepmerge] Merge existing simple keys in target at the roots', () => {
	const source = { key1: 'changed', key2: 'value2' };
	const target = { key1: 'value1', key3: 'value3' };

	expect(deepmerge(target, source)).toEqual({
		key1: 'changed',
		key2: 'value2',
		key3: 'value3'
	});
});

test('[deepmerge] Merge nested objects into target', () => {
	const source = { key1: { subkey1: 'changed', subkey3: 'added' } };
	const target = { key1: { subkey1: 'value1', subkey2: 'value2' } };

	expect(deepmerge(target, source)).toEqual({
		key1: {
			subkey1: 'changed',
			subkey2: 'value2',
			subkey3: 'added'
		}
	});
});

test('[deepmerge] Replace simple key with nested object in target', () => {
	const source = { key1: { subkey1: 'subvalue1', subkey2: 'subvalue2' } };
	const target = { key1: 'value1', key2: 'value2' };

	expect(deepmerge(target, source)).toEqual({
		key1: { subkey1: 'subvalue1', subkey2: 'subvalue2' },
		key2: 'value2'
	});
});

test('[deepmerge] Should add nested object in target', () => {
	const source = { b: { c: {} } };
	const target = { a: {} };

	expect(deepmerge(target, source)).toEqual({
		a: {},
		b: { c: {} }
	});
});

test('[deepmerge] Should clone source and target', () => {
	const source = { b: { c: 'foo' } };
	const target = { a: { d: 'bar' } };

	const merged = deepmerge(target, source, { clone: true });

	expect(merged).toEqual({
		a: { d: 'bar' },
		b: { c: 'foo' }
	});
	expect(merged.a).not.toBe(target.a);
	expect(merged.b).not.toBe(source.b);
});

test('[deepmerge] Should not clone source and target', () => {
	const source = { b: { c: 'foo' } };
	const target = { a: { d: 'bar' } };

	const merged = deepmerge(target, source);
	expect(merged).toEqual({
		a: { d: 'bar' },
		b: { c: 'foo' }
	});
	expect(merged.a).toBe(target.a);
	expect(merged.b).toBe(source.b);
});

test('[deepmerge] Should replace object with simple key in target', () => {
	const source = { key1: 'value1' };
	const target = { key1: { subkey1: 'subvalue1', subkey2: 'subvalue2' }, key2: 'value2' };

	expect(deepmerge(target, source)).toEqual({
		key1: 'value1',
		key2: 'value2'
	});
});

test('[deepmerge] Should replace objects with arrays', () => {
	const source = [{ key1: ['subkey'] }];
	const target = [{ key1: { subkey: 'one' } }];

	expect(deepmerge(target, source)).toEqual([
		{ key1: ['subkey'] }
	]);
});

test('[deepmerge] Should replace dates with arrays', () => {
	const source = [{ key1: ['subkey'] }];
	const target = [{ key1: new Date() }];

	expect(deepmerge(target, source)).toEqual([
		{ key1: ['subkey'] }
	]);
});

test('[deepmerge] Should replace null with arrays', () => {
	const source = { key1: ['subkey'] };
	const target = { key1: null };

	expect(deepmerge(target, source)).toEqual({
		key1: ['subkey']
	});
});

test('[deepmerge] Should work on simple array', () => {
	const source = ['one', 'three'];
	const target = ['one', 'two'];

	const merged = deepmerge(target, source);

	expect(merged).toEqual(['one', 'two', 'three']);
	expect(merged).toBeInstanceOf(Array);
});

test('[deepmerge] Should work on another simple array', () => {
	const source = ['t1', 's1', 'c2', 'r1', 'p2', 'p3'];
	const target = ['a1', 'a2', 'c1', 'f1', 'p1'];

	const merged = deepmerge(target, source);

	expect(merged).toEqual(['a1', 'a2', 'c1', 'f1', 'p1', 't1', 's1', 'c2', 'r1', 'p2', 'p3']);
	expect(merged).toBeInstanceOf(Array);
});

test('[deepmerge] Should work on array properties', () => {
	const source = { key1: ['one', 'three'], key2: ['four'] };
	const target = { key1: ['one', 'two'] };

	const merged = deepmerge(target, source);

	expect(merged).toEqual({
		key1: ['one', 'two', 'three'],
		key2: ['four']
	});
	expect(merged.key1).toBeInstanceOf(Array);
	expect(merged.key2).toBeInstanceOf(Array);
});

test('[deepmerge] Should work on array properties with clone option', () => {
	const source = { key1: ['one', 'three'], key2: ['four'] };
	const target = { key1: ['one', 'two'] };

	const merged = deepmerge(target, source, { clone: true });

	expect(merged.key1).not.toBe(source.key1);
	expect(merged.key1).not.toBe(target.key1);
	expect(merged.key2).not.toBe(source.key2);
});

test('[deepmerge] Should work on array of objects', () => {
	const source = [{ key1: ['one', 'three'], key2: ['one'] }, { key3: ['five'] }];
	const target = [{ key1: ['one', 'two'] }, { key3: ['four'] }];

	const merged = deepmerge(target, source);

	expect(merged).toEqual([
		{ key1: ['one', 'two', 'three'], key2: ['one'] },
		{ key3: ['four', 'five'] }
	]);

	expect(merged).toBeInstanceOf(Array);
	expect(merged[0].key1).toBeInstanceOf(Array);
});

test('[deepmerge] Should work on array of objects with clone option', () => {
	const source = [{ key1: ['one', 'three'], key2: ['one'] }, { key3: ['five'] }];
	const target = [{ key1: ['one', 'two'] }, { key3: ['four'] }];

	const merged = deepmerge(target, source, { clone: true });

	expect(merged).toEqual([
		{ key1: ['one', 'two', 'three'], key2: ['one'] },
		{ key3: ['four', 'five'] }
	]);
	expect(merged[0].key1).not.toBe(source[0].key1);
	expect(merged[0].key1).not.toBe(target[0].key1);
	expect(merged[0].key2).not.toBe(source[0].key2);
	expect(merged[1].key3).not.toBe(source[1].key3);
	expect(merged[1].key3).not.toBe(target[1].key3);
});

test('[deepmerge] Should work on arrays of nested objects', () => {
	const source = [{ key1: { subkey: 'two' } }, { key2: { subkey: 'three' } }];
	const target = [{ key1: { subkey: 'one' } }];

	expect(deepmerge(target, source)).toEqual([
		{ key1: { subkey: 'two' } },
		{ key2: { subkey: 'three' } }
	]);
});

test('[deepmerge] Should treat regular expressions like primitive values', () => {
	const source = { key1: /efg/ };
	const target = { key1: /abc/ };

	const merged = deepmerge(target, source);

	expect(merged).toEqual({ key1: /efg/ });
	expect(merged.key1.test('efg')).toBeTruthy();
});

test('[deepmerge] Should treat regular expressions like primitive values and should not clone even with clone option', () => {
	const source = { key1: /efg/ };
	const target = { key1: /abc/ };

	const merged = deepmerge(target, source, { clone: true });

	expect(merged.key1).toBe(source.key1);
});

test('[deepmerge] Should treat dates like primitives', () => {
	const source = { key: tuesday };
	const target = { key: monday };

	const merged = deepmerge(target, source);

	expect(merged).toEqual({ key: tuesday });
	expect(merged.key.valueOf()).toBe(tuesday.valueOf());
});

test('[deepmerge] Should treat dates like primitives and should not clone even with clone option', () => {
	const source = { key: tuesday };
	const target = { key: monday };

	const merged = deepmerge(target, source, { clone: true });

	expect(merged.key).toBe(tuesday);
});

test('[deepmerge] Should work on array with null in it', () => {
	const source = [null];
	const target = [];

	expect(deepmerge(target, source)).toEqual([null]);
});

test('[deepmerge] Should clone array\'s element if it is object', () => {
	const a = { key: 'yup' };
	const source = [a];
	const target = [];

	const merged = deepmerge(target, source, { clone: true });

	expect(merged).toEqual([{ key: 'yup' }]);
	expect(merged[0]).not.toBe(a);
	expect(merged[0].key).toBe('yup');
});

test('[deepclone] Should clone an array property when there is no target array', () => {
	const someObject = {};
	const source = { ary: [someObject] };
	const target = {};

	const merged = deepmerge(target, source, { clone: true });

	expect(merged).toEqual({ ary: [{}] });
	expect(merged.ary[0]).not.toBe(someObject);
});

test('[deepmerge] Should clone array property if there is no target property', () => {
	const source = { };
	const target = { key1: ['one'] };

	const merged = deepmerge(target, source, { clone: true });

	expect(merged).toEqual({ key1: ['one'] });
	expect(merged.key1).not.toBe(target.key1);
});

test('[deepclone] Should overwrite values when property is initialised but undefined', () => {
	const source = { value: undefined };		// eslint-disable-line no-undefined
	const target1 = { value: [] };
	const target2 = { value: null };
	const target3 = { value: 2 };

	expect(deepmerge(target1, source)).toHaveProperty('value', undefined); // eslint-disable-line no-undefined
	expect(deepmerge(target2, source)).toHaveProperty('value', undefined); // eslint-disable-line no-undefined
	expect(deepmerge(target3, source)).toHaveProperty('value', undefined); // eslint-disable-line no-undefined
});

test('[deepclone] null should be equal to null in an array', () => {
	const source = [null, 'lol'];
	const target = [null, 'dude'];

	const merged = deepmerge(target, source);

	expect(merged).toEqual([null, 'dude', 'lol']);
});

test('[deepclone] Dates in an array should be compared correctly', () => {
	const source = [monday, 'lol'];
	const target = [monday, 'dude'];

	const merged = deepmerge(target, source);

	expect(merged).toEqual([monday, 'dude', 'lol']);
});

test('[deepclone] Dates should copy correctly in an array', () => {
	const source = [tuesday, 'lol'];
	const target = [monday, 'dude'];

	const merged = deepmerge(target, source);

	expect(merged).toEqual([monday, 'dude', tuesday, 'lol']);
});

test('[deepclone] Custom merge array', () => {
	const source = { someArray: [1, 2, 3] };
	const target = {
		someArray: [1, 2],
		someObject: { what: 'yes' }
	};

	const arrayMerge = jest.fn((targetArray, sourceArray) => targetArray.concat(sourceArray));
	const merged = deepmerge(target, source, { arrayMerge });

	expect(arrayMerge).toHaveBeenCalledWith(
		[1, 2],
		[1, 2, 3],
		expect.objectContaining({ arrayMerge })
	);
	expect(merged).toEqual({
		someArray: [1, 2, 1, 2, 3],
		someObject: { what: 'yes' }
	});
});

test('[deepclone] Merge top-level arrays', () => {
	function concatMerge(a, b) {
		return a.concat(b);
	}
	const merged = deepmerge([1, 2], [1, 2], { arrayMerge: concatMerge });

	expect(merged).toEqual([1, 2, 1, 2]);
});
