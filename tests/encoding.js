/* eslint-disable no-magic-numbers */

import { base62 } from '../src/encoding';

test('[base62] 0 returns \'0\'', () => {
	expect(base62(0)).toBe('0');
});

test('[base62] Encode small numbers', () => {
	expect(base62(42)).toBe('G');
	expect(base62(4524)).toBe('1aY');
});

test('[base62] Encode big numers', () => {
	expect(base62(232342442)).toBe('fISSK');
	expect(base62(94665476783123123)).toBe('6ZzgNW9OOk');
});
