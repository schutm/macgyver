TARGET ?= $(CURDIR)
SRCDIR = $(firstword $(dir $(abspath $(MAKEFILE_LIST))))
OUTDIR = $(dir $(abspath $(TARGET))/)

REPORTDIR ?= $(OUTDIR)reports/
TESTDIR ?= $(OUTDIR)tests/
DISTDIR ?= $(OUTDIR)dist/
DEPDIR ?= $(OUTDIR)deps/

BUNDLED_DEPS =
VENDOR_DEPS =

NO_DOCS_TARGET = 1

include targets.mk
