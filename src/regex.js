/*
 * Return an array with all matches of <regex> against <string>.
 */
export function matchAll(regex, string) {
	const result = [];

	let matchResult;
	while ((matchResult = regex.exec(string)) !== null) {
		result.push(matchResult.slice());
	}

	return result;
}
