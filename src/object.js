/* Return an new object with <source> merged into <target>-that is- keys in target are overwritten
 * by keys in source. If a key exist only in target it is kept, if a key only exist in source it is
 * kept as well. Optional argument options are given in a hash where <arrayMerge> can be used to
 * overwrite the default array merge behaviour of combining two arrays and <clone> can be used to
 * indicate whether objects need to be cloned or referenced.
 *
 * The implementation is based on https://github.com/KyleAMathews/deepmerge.
 */
export function deepmerge(target, source, { arrayMerge = defaultArrayMerge, clone = false } = {}) {
	const options = { arrayMerge, clone };

	if (Array.isArray(source)) {
		return arrayMerge(target, source, options);
	} else if (isMergeableObject(source)) {
		return mergeObject(target, source, options);
	}

	return source;
}

const NOT_FOUND = -1;
function isMergeableObject(value) {
	const nonNullObject = value && typeof value === 'object';

	return nonNullObject
		&& Object.prototype.toString.call(value) !== '[object RegExp]'
		&& Object.prototype.toString.call(value) !== '[object Date]';
}

function emptyTarget(value) {
	return Array.isArray(value) ? [] : {};
}

function maybeClone(value, options) {
	return options.clone ? deepmerge(emptyTarget(value), value, options)
	             : value;
}

function mergeObject(target, source, options) {
	if (!isMergeableObject(target)) {
		return maybeClone(source, options);
	}

	const destination = Object.keys(source).reduce(
		(accumulator, key) => Object.assign({}, accumulator, {
			[key]: deepmerge(target[key], source[key], options)
		}), {}
	);

	return Object.keys(target).reduce(
		(accumulator, key) => {
			if (key in accumulator) {
				return accumulator;
			}

			return Object.assign({}, accumulator, {
				[key]: maybeClone(target[key], options)
			});
		}, destination);
}

function defaultArrayMerge(target, source, options) {
	/* eslint-disable no-param-reassign */
	return source.reduce((accumulator, value, i) => {
		if (typeof accumulator[i] === 'undefined') {
			accumulator[i] = maybeClone(value, options);
		} else if (isMergeableObject(value)) {
			accumulator[i] = deepmerge(accumulator[i], value, options);
		} else if (target.indexOf(value) === NOT_FOUND) {
			accumulator.push(maybeClone(value, options));
		}

		return accumulator;
	}, Array.isArray(target) ? target.slice() : []);
	/* eslint-enable no-param-reassign */
}
