/* eslint-disable no-bitwise, no-magic-numbers */

/*
 * Tranform <string> into a djb2 hash.
 *
 * The implementation is based on https://github.com/contra/djb2
 */
export function djb2(string) {
	const chars = string.split('');
	const hash = chars.reduce((accumulator, currentValue) =>
		((accumulator << 5) + accumulator) + currentValue.charCodeAt(0)
		, 5381);

	// JavaScript does bitwise operations (like XOR, above) on 32-bit signed
	// integers. Since we want the results to be always positive, convert the
	// signed int to an unsigned by doing an unsigned bitshift.
	return hash >>> 0;
}
