/*
 * Transform a function <fn>, taking multiple arguments, into a function that takes fewer arguments.
 * The initial passed arguments <args> are already applied. Another function is returned if more
 * arguments needed or the result if all arguments are supplied. Once a function <fn> is curried a
 * function is returned as long as more arguments are required to return a result.
 */
export function curry(fn, ...args) {
	const curriedFunction = (...fnArgs) => {
		if (fnArgs.length >= fn.length) {
			return fn(...fnArgs);
		}

		return (...cArgs) => curriedFunction(...fnArgs, ...cArgs);
	};

	return curriedFunction(...args);
}

/*
 * Apply the arguments <args> to the function <fn> and return a function taking the remaining
 * arguments to return the result.
 */
export function partial(fn, ...args) {
	return (...pArgs) => fn(...args, ...pArgs);
}
