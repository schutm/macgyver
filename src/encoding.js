const BASE = 62;
const CHARACTER_SET = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

/*
 * Encode the non-negative integer <nonNegativeInteger> into a base62 string.
 *
 * The implementation is based on https://github.com/andrew/base62.js
 */
export function base62(nonNegativeInteger) {
	let str = '';

	do {
		str = CHARACTER_SET[nonNegativeInteger % BASE] + str;

		// eslint-disable-next-line no-param-reassign
		nonNegativeInteger = Math.floor(nonNegativeInteger / BASE);
	} while (nonNegativeInteger > 0);			// eslint-disable-line no-magic-numbers

	return str;
}
