# Make sure not to build in the source directory
ifeq ($(SRCDIR),$(OUTDIR))
TARGET = ../output
endif

.PRECIOUS: $(DEPDIR)%

#=======================================================================================================================
# Dependencies
#=======================================================================================================================
DEP_REPO_macgyver = https://schutm.gitlab.io/macgyver/macgyver.latest.tar.bz2
DEP_REPO_mithril = git://github.com/MithrilJS/mithril.js.git next
DEP_REPO_material-icons = git://github.com/google/material-design-icons.git master

define dependencies
	$(eval $(call dependencies_$1,.$2-dependencies,$3))
endef

define dependencies_mithril
$(1): $(2)mithril.min.js

$(2)mithril.min.js: $(DEPDIR)mithril/mithril.min.js
	@echo "Copying '$$<' to '$$@'"
	@mkdir -p $$(@D)
	@cp $$< $$@

$(DEPDIR)mithril/mithril.min.js:: $(DEPDIR)mithril
	@echo -n # Stop rule matching
endef

define dependencies_substance
$(call dependencies,material-icons,$(1),$(2))
$(1): .$(1)-dependencies
$(1): $(2)substance.iife.min.js

$(2)substance.iife.min.js: $(DISTDIR)substance.iife.min.js
	@echo "Copying '$$<' to '$$@'"
	@mkdir -p $$(@D)
	@cp $$< $$@

$(DISTDIR)substance.iife.min.js:: dist
	@echo -n # Stop rule matching
endef

define dependencies_material-icons
$(1): $(2)MaterialIcons-Regular.eot
$(1): $(2)MaterialIcons-Regular.woff2
$(1): $(2)MaterialIcons-Regular.woff
$(1): $(2)MaterialIcons-Regular.ttf

$(2)MaterialIcons-Regular.eot: $(DEPDIR)material-icons/iconfont/MaterialIcons-Regular.eot
	@echo "Copying '$$<' to '$$@'"
	@mkdir -p $$(@D)
	@cp $$< $$@

$(DEPDIR)material-icons/iconfont/MaterialIcons-Regular.eot:: $(DEPDIR)material-icons
	@echo -n # Stop rule matching

$(2)MaterialIcons-Regular.woff: $(DEPDIR)material-icons/iconfont/MaterialIcons-Regular.woff
	@echo "Copying '$$<' to '$$@'"
	@mkdir -p $$(@D)
	@cp $$< $$@

$(DEPDIR)material-icons/iconfont/MaterialIcons-Regular.woff:: $(DEPDIR)material-icons
	@echo -n # Stop rule matching

$(2)MaterialIcons-Regular.woff2: $(DEPDIR)material-icons/iconfont/MaterialIcons-Regular.woff2
	@echo "Copying '$$<' to '$$@'"
	@mkdir -p $$(@D)
	@cp $$< $$@

$(DEPDIR)material-icons/iconfont/MaterialIcons-Regular.woff2:: $(DEPDIR)material-icons
	@echo -n # Stop rule matching

$(2)MaterialIcons-Regular.ttf: $(DEPDIR)material-icons/iconfont/MaterialIcons-Regular.ttf
	@echo "Copying '$$<' to '$$@'"
	@mkdir -p $$(@D)
	@cp $$< $$@

$(DEPDIR)material-icons/iconfont/MaterialIcons-Regular.ttf:: $(DEPDIR)material-icons
	@echo -n # Stop rule matching
endef


export YARN_SILENT := 1
export NODE_PATH := $(DEPDIR):/usr/local/share/.config/yarn/global/node_modules/

.SUFFIXES:

find = $(patsubst $(1)/%,%,$(wildcard $(1)/$(2)) $(wildcard $(1)/**/$(2)))
yarn = yarn run $(1) $(2); EXIT_CODE=$$?; rm -rf node_modules; exit $${EXIT_CODE}

SRCS = $(addprefix src/,$(call find,$(SRCDIR)src,*.js))
TESTS = $(addprefix test/,$(call find,$(SRCDIR)test,*.js))


#=======================================================================================================================
# Clean generated files
#=======================================================================================================================
.PHONY: clean
clean:
	@rm -rf $(OUTDIR)

.PHONY: mostlyclean
mostlyclean:
	@find $(OUTDIR) -mindepth 1 -maxdepth 1 -type d ! -path $(DEPDIR:/=) -exec rm -rf {} \;


#=======================================================================================================================
# Linting of the sources
#=======================================================================================================================
ifndef NO_LINT_TARGET
.PHONY: lint
lint: .lint-dependencies
	@$(call yarn,lint)

.PHONY: .lint-dependencies
.lint-dependencies:

.PHONY: lint-ci
lint-ci: .lint-ci-dependencies | $(abspath $(REPORTDIR)lint)/
	-@$(call yarn,lint -f html -o $(abspath $(REPORTDIR)lint/index.html))
	-@$(call yarn,lint --no-color | tee $(abspath $(REPORTDIR)lint.out))

.PHONY: .lint-ci-dependencies
.lint-ci-dependencies:
endif


#=======================================================================================================================
# Testing
#=======================================================================================================================
ifndef NO_TEST_TARGET
.PHONY: test
test: .test-dependencies
	-@$(call yarn,test,$(if $(UPDATE_SNAPSHOTS),-u))

.PHONY: .test-dependencies
.test-dependencies: .dependencies

.PHONY: test-coverage
test-coverage: .test-coverage-dependencies
	@$(call yarn,test:coverage,--coverageDirectory=$(TESTDIR))

.PHONY: .test-coverage-dependencies
.test-coverage-dependencies: .dependencies

.PHONY: test-ci
test-ci: .test-ci-dependencies | $(abspath $(REPORTDIR)coverage)/ $(abspath $(REPORTDIR)tests)/
	-@($(call yarn,test:coverage,--coverageDirectory=$(REPORTDIR)/coverage\
	                             --coverageReporters=html --coverageReporters=text-summary\
	                             --ci \
	                             --json)) | { tee /dev/fd/3 \
	                                          | grep -v '{.*}' > $(REPORTDIR)coverage.out ; \
	                                        } 3>&1 \
	                                      | jest-json-to-tap | { tee /dev/fd/4 \
	                                                             | tap-that-html > $(REPORTDIR)tests/index.html ; \
	                                                           } 4> $(REPORTDIR)tap.out

.PHONY: .test-ci-dependencies
.test-ci-dependencies: .dependencies
endif


#=======================================================================================================================
# Documentation
#=======================================================================================================================
ifndef NO_DOCS_TARGET
.PHONY: docs
docs: .docs-dependencies
	@echo "Creating documentation"
	@mkdocs build -q -f $(SRCDIR)mkdocs.yml -d $(DOCDIR)

.PHONY: .docs-dependencies
.docs-dependencies:
endif


#=======================================================================================================================
# Distribution creation
#=======================================================================================================================
ifndef NO_DIST_TARGET
.PHONY: dist
dist: .dist-dependencies
	@echo "Creating distribution packages"
	@cd $(SRCDIR) && rollup -c --silent --environment outdir:$(DISTDIR),depdir:$(DEPDIR)

.PHONY: .dist-dependencies
.dist-dependencies: .dependencies
.dist-dependencies: $(SRCDIR)rollup.config.js
.dist-dependencies: $(addprefix $(SRCDIR),$(SRCS))
endif


#=======================================================================================================================
# Helper targets
#=======================================================================================================================
.PHONY: .dependencies
.dependencies: .bundled-dependencies
.dependencies: .vendor-dependencies

.PHONY: .bundled-dependencies
.bundled-dependencies: $(addprefix $(DEPDIR),$(BUNDLED_DEPS))

.PHONY: .vendor-dependencies
.vendor-dependencies: $(addprefix $(DEPDIR),$(VENDOR_DEPS))

$(DEPDIR)%:
	@echo "Installing dependency $* from $(DEP_REPO_$*)"
	$(call fetch_$(firstword $(subst :, ,$(DEP_REPO_$*))),$(DEP_REPO_$*),$(DEPDIR)/$*)

%/:
	-@mkdir -p $@

define fetch_file
	@mkdir -p $(2) && \
	cp -r $(patsubst file://%,%,$(1))/* $(2)
endef

define fetch_git
	@git clone -q -n -- $(patsubst git://%,https://%,$(firstword $(1))) $(2) && \
	cd $(2) && git checkout -q $(lastword $(1))
endef

define fetch_https
	@mkdir -p $(2) && \
	cd $@ && curl -sL $(1) | tar xj
endef
