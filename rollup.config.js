/* global process:false */

import buble from 'rollup-plugin-buble';
import multiEntry from 'rollup-plugin-multi-entry';
import pkg from './package.json';
import { dirname } from 'path';

const outdir = process.env.outdir || dirname(pkg.main || pkg.module || pkg['jsnext:main']);

export default {
	entry: 'src/**/*.js',
	plugins: [
		buble(),
		multiEntry()
	],
	output: [
		{
			format: 'cjs',
			file: `${outdir}/${pkg.name}.cjs.js`,
			sourcemap: true
		},
		{
			format: 'es',
			file: `${outdir}/${pkg.name}.es.js`,
			sourcemap: true
		}
	]
};
